'Application to calculate
'area and perimeter of a rect
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 11 / 2021
'License  => Cardware


DIM L1 AS SINGLE
DIM L2 AS SINGLE
DIM PERIMETER AS SINGLE
DIM AREA AS SINGLE


PRINT  "INSERT THE SIDE  L1 OF THE RECT : "
INPUT L1

PRINT "INSERT THE SIDE L2 OF THE RECT : "
INPUT L2

PERIMETER = 2 * L1 + 2 * L2
AREA = L1 * L2

PRINT "PERIMETER = ", PERIMETER
PRINT  "AREA = ", AREA

