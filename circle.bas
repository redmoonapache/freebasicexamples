'Application to calculate a circle
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 11 / 2021
'License  => Cardware

DIM PIGR   AS SINGLE
DIM RADIUS AS SINGLE
DIM PERIM  AS SINGLE
DIM AREA   AS SINGLE

PRINT "CALCOLOUS OF A CIRCLE"

PIGR = 3.14159

PRINT "WRITE THE RADIUS ... "
INPUT RADIUS

AREA = PIGR * RADIUS * RADIUS
PERIM = 2 * PIGR * RADIUS

PRINT " PERIMETER = ", PERIM
PRINT " AREA = ", AREA
