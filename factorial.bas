'Application to calculate a factorial
'                           number
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 11 / 11 / 2021
'License  => Cardware


DIM N         AS INTEGER
DIM I         AS INTEGER
DIM FACTORIAL AS INTEGER


PRINT "CALCOLOUS OF FACTORIAL OF N"

PRINT "WRITE THE NUMBER   N    ... "
INPUT N

FACTORIAL = 1


FOR I = 1 TO N
   FACTORIAL = FACTORIAL * I
NEXT I


PRINT " FACTORIAL OF ", N , " = ", FACTORIAL



