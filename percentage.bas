'Application to calculate percentage
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 21 / 12 / 2021
'License  => Cardware

DIM PERCENT1 AS SINGLE
DIM TOTAL1   AS SINGLE
DIM RESULT1  AS SINGLE

PRINT "CALCOLOUS OF THE PERCENTAGE"


PRINT "WRITE THE PERCENT ... "
INPUT PERCENT1

PRINT "WRITE THE TOTAL ... "
INPUT TOTAL1

RESULT1 = PERCENT1 / 100 * TOTAL1


PRINT PERCENT1; " % OF "; TOTAL1; " = "; RESULT1
PRINT ""

