'Application to do calculation
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 02 / 02 / 2022
'License  => Cardware

DIM DATA1     AS SINGLE
DIM DATA2     AS SINGLE
DIM OPERATOR1 AS INTEGER
DIM RESULT1   AS SINGLE

PRINT "CALCOLATIONS   ..."
PRINT ""
PRINT "WRITE DATA 1 : "
INPUT DATA1

PRINT ""
PRINT "WRITE DATA 2 : "
INPUT DATA2

PRINT ""
PRINT " 1 FOR +     2 FOR - "
PRINT " 3 FOR *     4 FOR / "
PRINT ""
INPUT OPERATOR1

Select Case As Const OPERATOR1
Case 1
    RESULT1 = DATA1 + DATA2
    PRINT DATA1; " + "; DATA2; " = "; RESULT1
Case 2
    RESULT1 = DATA1 - DATA2
    PRINT DATA1; " - "; DATA2; " = "; RESULT1
Case 3
    RESULT1 = DATA1 * DATA2
    PRINT DATA1; " * "; DATA2; " = "; RESULT1
Case 4
    RESULT1 = DATA1 / DATA2
    PRINT DATA1; " / "; DATA2; " = "; RESULT1
End Select


