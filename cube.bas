'Application to calculate volume
'               of a cube
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 11 / 2021
'License  => Cardware

DIM SIDE   AS SINGLE
DIM VOLUME AS SINGLE


PRINT "CALCULATION  OF THE VOLUME OF A CUBE ... "
PRINT "INSERT THE SIDE"
INPUT SIDE

VOLUME = SIDE * SIDE * SIDE

PRINT "VOLUME = ", VOLUME

