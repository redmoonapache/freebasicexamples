'Application to convert number
'from binary to decimal
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 11 / 2021
'License  => Cardware

DIM NUMSTR AS STRING
DIM LENGHTBIN AS INTEGER
DIM NUMDEC AS INTEGER
DIM POSIZ AS INTEGER
DIM I AS INTEGER
DIM CIFRASTR AS STRING
DIM CIFRADEC AS INTEGER


PRINT "input the number you want in binary system : "
INPUT NUMSTR

LENGHTBIN = LEN(NUMSTR)

NUMDEC = 0

FOR I = 0 TO (LENGHTBIN - 1)

	POSIZ = LENGHTBIN - I
		
	CIFRASTR = MID$(NUMSTR, POSIZ, 1)
	
	CIFRADEC = VAL(CIFRASTR)
	
	NUMDEC = NUMDEC +  (2 ^ I) * CIFRADEC
			 

NEXT I

PRINT "number in decimal system  =   ", NUMDEC
	 
