'Application to calculate a volume of a sphere
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 24 / 11 / 2021
'License  => Cardware

DIM PIGR   AS SINGLE
DIM RADIUS AS SINGLE
DIM VOLUME AS SINGLE

PRINT "CALCOLOUS OF A VOLUME OF A SPHERE ..."

PIGR = 3.14159

PRINT "WRITE THE RADIUS ... "
INPUT RADIUS

VOLUME = 4 / 3 * PIGR * ( RADIUS * RADIUS * RADIUS )

PRINT " VOLUME = ", VOLUME

