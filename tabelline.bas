'Application to play with numbers
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 12 / 2021
'License  => Cardware


dim num1 as double
dim num2 as double
dim num1i as integer
dim num2i as integer
dim risult1 as integer
dim risp1 as integer
dim answ as string
dim totale as integer
dim giuste as integer
dim sbagliate as integer

open "errata.txt" for output as #1


totale = 0
giuste = 0


do

cls

randomize
num1 = rnd()

randomize
num2 = rnd()


num1 = num1 * 10
num2 = num2 * 10

totale = totale + 1


num1i = cint(num1)
num2i = cint(num2)

risult1 = num1i * num2i

print num1i; " x "; num2i; " = ? "
print ""
print "inserire il risultato ..."
print ""
input risp1
print ""

if (risp1 = risult1) then
   print "ESATTO : "; num1i; " x "; num2i; " = "; risult1
   giuste = giuste + 1
   
else
   print "ERRORE : "; num1i; " x "; num2i; " = "; risult1
   print #1, "ERRORE : "; num1i; " x "; num2i; " <> "; risp1 ; " = "; risult1
   
endif

print ""


print "Ancora ( y / n ) :"
input answ

print ""

loop until (answ = "n")


print "interrogazioni = "; totale
print "di cui giuste  = "; giuste
sbagliate = totale - giuste
print "e per controllo : sbagliate = "; sbagliate

close #1


