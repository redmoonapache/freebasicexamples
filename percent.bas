'Application to calculate percent
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 10 / 11 / 2021
'License  => Cardware


DIM TOTAL AS SINGLE
DIM PERC  AS SINGLE
DIM PART  AS SINGLE


PRINT "CALCULUS OF % "
PRINT "WRITE TOTAL AMOUNT ... "
INPUT TOTAL

PRINT "WRITE PERCENT ... "
INPUT PERC

PART = PERC / 100 * TOTAL

PRINT "PERCENT ", PERC, " / 100  ON A TOTAL OF ", TOTAL , " = ", PART

 
