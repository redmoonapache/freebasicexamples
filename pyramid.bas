'Application to calculate a volume of a pyramid
'
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 04 / 01 / 2022
'License  => Cardware

DIM SIDE1   AS SINGLE
DIM HEIGHT1 AS SINGLE
DIM VOLUME1 AS SINGLE


PRINT "CALCOLOUS OF A PYRAMID WITH SQUARE BASE"


PRINT "WRITE THE SIDE ... "
INPUT SIDE1

PRINT "WRITE THE HEIGHT ... "
INPUT HEIGHT1

VOLUME1 = SIDE1 * SIDE1 * HEIGHT1 / 3


PRINT " VOLUME   = "; VOLUME1

PRINT " Done."

