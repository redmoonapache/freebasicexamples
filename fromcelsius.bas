'Application to calculate temperature
' from celsius
'              
'Author   => Mattia Tristo
'EMail    => mattia.tristo@aol.com
'Date     => 05 / 01 / 2022
'License  => Cardware

DIM CELSIUS    AS SINGLE
DIM FAHRENHEIT AS SINGLE
DIM KELVIN     AS SINGLE
DIM RANKINE    AS SINGLE


PRINT "CALCULATION  OF THE TEMPERATURE ... "
PRINT "INSERT CELSIUS ..."
INPUT CELSIUS

FAHRENHEIT = ( CELSIUS * 9 / 5 ) + 32 
KELVIN     = CELSIUS + 273.15
RANKINE    = FAHRENHEIT + 459.67


PRINT ""
PRINT "CELSIUS    = "; CELSIUS
PRINT "FAHRENHEIT = "; FAHRENHEIT
PRINT "KELVIN     = "; KELVIN
PRINT "RANKINE    = "; RANKINE

PRINT "Done."




